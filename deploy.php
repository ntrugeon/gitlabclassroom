<?php
namespace Deployer;

require 'recipe/symfony.php';
require 'contrib/npm.php';
require 'contrib/webpack_encore.php';

// Config

set('repository', getenv('CI_REPOSITORY_URL'));

add('shared_dirs', [
    'var/log',
    'public/studentResults'
]);

// Hosts

host('gitlabclassroom')
    ->set('hostname', getenv('SERVER_HOSTNAME'))
    ->set('remote_user', getenv('SERVER_REMOTE_USER'))
    ->set('keep_releases', 5)
    ->set('deploy_path', getenv('SERVER_DEPLOY_PATH'));

// Hooks
after('deploy:vendors', 'npm:install');
after('deploy:vendors', 'webpack_encore:build');
after('deploy:failed', 'deploy:unlock');
