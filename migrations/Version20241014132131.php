<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241014132131 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE exercise (id INT AUTO_INCREMENT NOT NULL, assignment_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', student_id INT DEFAULT NULL, gitlab_project_id INT NOT NULL, gitlab_project_url VARCHAR(255) NOT NULL, INDEX IDX_AEDAD51CD19302F8 (assignment_id), INDEX IDX_AEDAD51CCB944F1A (student_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE exercise ADD CONSTRAINT FK_AEDAD51CD19302F8 FOREIGN KEY (assignment_id) REFERENCES assignment (id)');
        $this->addSql('ALTER TABLE exercise ADD CONSTRAINT FK_AEDAD51CCB944F1A FOREIGN KEY (student_id) REFERENCES user (id)');
        $this->addSql('DROP INDEX `primary` ON assignment_user');
        $this->addSql('ALTER TABLE assignment_user ADD PRIMARY KEY (assignment_id, user_id)');
        $this->addSql('ALTER TABLE assignment_user RENAME INDEX idx_97a28256d19302f8 TO IDX_D6951BD5D19302F8');
        $this->addSql('ALTER TABLE assignment_user RENAME INDEX idx_97a28256a76ed395 TO IDX_D6951BD5A76ED395');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE exercise DROP FOREIGN KEY FK_AEDAD51CD19302F8');
        $this->addSql('ALTER TABLE exercise DROP FOREIGN KEY FK_AEDAD51CCB944F1A');
        $this->addSql('DROP TABLE exercise');
        $this->addSql('DROP INDEX `PRIMARY` ON assignment_user');
        $this->addSql('ALTER TABLE assignment_user ADD PRIMARY KEY (user_id, assignment_id)');
        $this->addSql('ALTER TABLE assignment_user RENAME INDEX idx_d6951bd5d19302f8 TO IDX_97A28256D19302F8');
        $this->addSql('ALTER TABLE assignment_user RENAME INDEX idx_d6951bd5a76ed395 TO IDX_97A28256A76ED395');
    }
}
