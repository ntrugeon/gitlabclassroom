<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241013121431 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE assignment_user (user_id INT NOT NULL, assignment_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_97A28256A76ED395 (user_id), INDEX IDX_97A28256D19302F8 (assignment_id), PRIMARY KEY(user_id, assignment_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE assignment_user ADD CONSTRAINT FK_97A28256A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE assignment_user ADD CONSTRAINT FK_97A28256D19302F8 FOREIGN KEY (assignment_id) REFERENCES assignment (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE assignment_user DROP FOREIGN KEY FK_97A28256A76ED395');
        $this->addSql('ALTER TABLE assignment_user DROP FOREIGN KEY FK_97A28256D19302F8');
        $this->addSql('DROP TABLE assignment_user');
    }
}
