<?php

namespace App\Form;

use App\Entity\Classroom;
use App\Service\GitlabConnector;
use Gitlab\ResultPager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\UX\Autocomplete\Form\AsEntityAutocompleteField;

#[AsEntityAutocompleteField]
class ClassroomType extends AbstractType
{
    public function __construct(private GitlabConnector $connector)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $pager = new ResultPager($this->connector->getClient());
        // TODO: top_level ou uniquement à partir d'un certain niveau
        $groups = $pager->fetchAll($this->connector->getClient()->groups(), 'all', [['owned' => true]]);
        $groupsObjects = [];
        $groupsObjects[0] = (object) ['id' => 0, 'name' => '--- No group parent ---'];
        foreach ($groups as $group) {
            $groupsObjects[] = (object) $group;
        }
        $builder
            ->add('name', TextType::class, [
                'attr' => [
                    'class' => 'block w-full rounded-md border-0 py-1.5 text-dark-1 shadow-sm ring-1 ring-inset
                    ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600
                    sm:text-sm sm:leading-6',
                    'placeholder' => 'Classroom Name'
                ],
                'label' => 'Classroom name',
                'label_attr' => ['class' => 'block text-sm font-medium leading-6 text-gray-900'],
            ])
            ->add('parentGroup', ChoiceType::class, [
                'choices' => $groupsObjects,
                'choice_value' => 'id',
                'choice_label' => 'name',
                'mapped' => false,
                'autocomplete' => true,
                'attr' => [
                    'class' => 'text-dark-1',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Classroom::class,
        ]);
    }
}
