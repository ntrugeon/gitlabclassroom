<?php

namespace App\Form;

use App\Entity\Assignment;
use App\Service\GitlabConnector;
use Gitlab\ResultPager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AssignmentType extends AbstractType
{
    public function __construct(private GitlabConnector $connector)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if (!$options['is_edit']) {
            $pager = new ResultPager($this->connector->getClient());
            $id = $options['gitlabId'];
            $templateProjects = $pager->fetchAll($this->connector->getClient()->groups(), 'projects', [$id]);
            $templateProjectsObjects = [];
            $templateProjectsObjects[0] = (object)['id' => 0, 'name' => '--- No template ---'];
            foreach ($templateProjects as $templateProject) {
                $templateProjectsObjects[] = (object)$templateProject;
            }
        }
        if (!$options['is_edit']) {
            $builder
                ->add('name', TextType::class, [
                    'attr' => [
                        'class' => 'block w-full rounded-md border-0 py-1.5 text-dark-1 shadow-sm
                    ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2
                    focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6',
                        'placeholder' => 'Name of the assignment (eg : Exercise 101)'
                    ],
                    'label' => 'Assignment name',
                    'label_attr' => ['class' => 'block text-sm font-medium leading-6 text-gray-900'],
                ])
                ->add('template', ChoiceType::class, [
                    'choices' => $templateProjectsObjects,
                    'choice_value' => 'id',
                    'choice_label' => 'name',
                    'mapped' => false,
                    'attr' => [
                        'class' => 'block w-full rounded-md border-0 py-1.5 text-dark-1 shadow-sm ring-1
                    ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset
                    focus:ring-indigo-600 sm:text-sm sm:leading-6',
                    ],
                    'label' => 'GitLab Repository Template',
                    'label_attr' => [
                        'class' => 'block text-sm font-medium leading-6 text-gray-900'
                    ],
                    'autocomplete' => true,
                ]);
        }
            $builder->add('startDate', DateType::class, [
                'widget' => 'single_text',
                'required' => false,
                'attr' => [
                    'class' => 'block w-full rounded-md border-0 py-1.5 text-dark-1 shadow-sm
                    ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2
                    focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6',
                    'placeholder' => 'Start date of the assignment'
                ],
                'label' => 'Start date',
                'label_attr' => ['class' => 'block text-sm font-medium leading-6 text-gray-900'],
            ])
            ->add('endDate', DateType::class, [
                'widget' => 'single_text',
                'required' => false,
                'attr' => [
                    'class' => 'block w-full rounded-md border-0 py-1.5 text-dark-1 shadow-sm
                    ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2
                    focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6',
                    'placeholder' => 'End date of the assignment'
                ],
                'label' => 'End date',
                'label_attr' => ['class' => 'block text-sm font-medium leading-6 text-gray-900'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Assignment::class,
            // default value for the gitlabId option from controller to formtype
            'gitlabId' => null,
            'is_edit' => false
        ]);
        $resolver->setAllowedTypes('is_edit', 'bool');
    }
}
