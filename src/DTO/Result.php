<?php

namespace App\DTO;

use Exception;

class Result
{
    private ?string $filename;

    private ?int $score;

    private ?int $total;

    private ?bool $success = true;

    private ?\SimpleXMLElement $content = null;

    private ?array $testcases;

    /**
     * @param string|null $filename
     */
    public function __construct(?string $filename)
    {
        $this->testcases = [];
        $this->filename = $filename;
        if (!file_exists($this->filename) || !is_file($this->filename)) {
            $this->success = false;
        } else {
            $this->content = simplexml_load_file($this->filename);
        }
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(?int $score): void
    {
        $this->score = $score;
    }

    public function getTotal(): ?int
    {
        return $this->total;
    }

    public function setTotal(?int $total): void
    {
        $this->total = $total;
    }

    public function isSuccess(): ?bool
    {
        return $this->success;
    }

    public function getContent(): ?\SimpleXMLElement
    {
        return $this->content;
    }

    public function getTestcases(): ?array
    {
        return $this->testcases;
    }

    /**
     * Processes the XML content and generates test case data for display in the view.
     * Extracts test case details from the XML structure, removes unnecessary HTML tags,
     * and transforms the extracted data into `TestCase` DTOs with their respective statuses and failure/error messages.
     */
    public function getContentToView()
    {
        $xml = $this->getContent();
        if ($xml !== null) {
            foreach ($xml->xpath('//testcase') as $testcase) {
                $testcaseString = str_replace('&lt;!DOCTYPE html&gt;', '', $testcase->asXML());
                $testcaseString = preg_replace('/&lt;html.*&gt;/m', '', $testcaseString);
                $testcaseString = preg_replace('/&lt;\/?html&gt;/', '', $testcaseString);
                $testcaseString = preg_replace('/&lt;body&gt;/', '', $testcaseString);
                $testcaseString = preg_replace('/&lt;\/body&gt;/', '', $testcaseString);
                $testcaseString = preg_replace('/&lt;head&gt;.*&lt;\/head&gt;/s', '', $testcaseString);
                $testcase = simplexml_load_string($testcaseString);
                $testcaseDTO = new TestCase($testcase['name']);
                if (isset($testcase->failure)) {
                    $testcaseDTO->setStatus('danger');
                    $testcaseDTO->setFailure(nl2br($testcase->failure));
                } elseif (isset($testcase->error)) {
                    $testcaseDTO->setStatus('danger');
                    $testcaseDTO->setError(nl2br($testcase->error));
                } else {
                    $testcaseDTO->setStatus('success');
                }
                $this->testcases[] = $testcaseDTO;
            }
        }
    }

    public function calculateScore()
    {
        $xml = $this->getContent();
        $testsuite = $xml->xpath('//testsuite');
        $results = [];
        foreach ($testsuite[0]->attributes() as $name => $value) {
            $results[$name] = $value;
        }

        $total = 0;
        $successful = 0;

        $total += (int) $results['tests'];
        $failed = (int) $results['failures'] + (int) $results['errors'];
        $successful += $total - $failed;

        if ($total === 0) {
            $this->success = false; // Aucun test n'a été effectué
        } else {
            $this->setScore($successful);
            $this->setTotal($total);
        }
    }
}
