<?php

namespace App\DTO;

class TestCase
{
    private ?string $name;

    private ?string $status = "success";

    private ?string $error = null;

    private ?string $failure = null;

    /**
     * @param string|null $name
     */
    public function __construct(?string $name)
    {
        $this->name = $name;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): void
    {
        $this->status = $status;
    }

    public function getError(): ?string
    {
        return $this->error;
    }

    public function setError(?string $error): void
    {
        $this->error = $error;
    }

    public function getFailure(): ?string
    {
        return $this->failure;
    }

    public function setFailure(?string $failure): void
    {
        $this->failure = $failure;
    }
}
