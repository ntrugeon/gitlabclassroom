<?php

namespace App\EventSubscriber;

use App\Entity\User;
use App\Event\ClassroomEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

final readonly class ClassroomNotificationSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private MailerInterface $mailer,
        private TranslatorInterface $translator,
    ) {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            ClassroomEvent::class => 'onClassroomCreated',
        ];
    }

    public function onClassroomCreated(ClassroomEvent $event): void
    {
        $classroom = $event->getClassroom();
        $action = $this->translator->trans($event->getMessage());

        /** @var User $creator */
        $creator = $classroom->getOwner();

        $subject = "Classroom {$action}: {$classroom->getName()}";

        $body = "<h1>{$this->translator->trans("hello")} !</h1>"
            . "<p>{$this->translator->trans('email_classroom_action', [
                'name' => $classroom->getName(),
                'action' => $action,
                'creator' => $creator->getUserIdentifier(),
                ])}.</p>"
            . "<p>{$this->translator->trans("Best regards")}</p>"
            . "<p>{$this->translator->trans("classroom_manager")}</p>";

        // See https://symfony.com/doc/current/mailer.html
        $email = (new Email())
            ->from("ntrugeon@univ-lr.fr")
            ->to("ntrugeon@univ-lr.fr")
            ->subject($subject)
            ->html($body)
        ;

        $this->mailer->send($email);
    }
}
