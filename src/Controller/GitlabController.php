<?php

declare(strict_types=1);

namespace App\Controller;

use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class GitlabController extends AbstractController
{
    #[Route('/connect/gitlab', name: 'connect_gitlab')]
    public function connect(ClientRegistry $clientRegistry): Response
    {
        // will redirect to Gitlab!
        return $clientRegistry
            ->getClient('gitlab') // key used in config/packages/knpu_oauth2_client.yaml
            ->redirect(['api'], []);
    }

    /**
     * After going to Gitlab, you're redirected back here
     * because this is the "redirect_route" you configured
     * in config/packages/knpu_oauth2_client.yaml
     */
    #[Route('/connect/gitlab/check', name: 'connect_gitlab_check')]
    public function connectCheck(Request $request, ClientRegistry $clientRegistry)
    {
        // ** if you want to *authenticate* the user, then
        // leave this method blank and create a Guard authenticator
        // (read below)

        /** @var \KnpU\OAuth2ClientBundle\Client\Provider\GitlabClient $client */
        $client = $clientRegistry->getClient('gitlab');

        try {
            $accessToken = $client->getAccessToken();
            if ($accessToken->hasExpired()) {
                $accessToken = $client->refreshAccessToken($accessToken->getRefreshToken());
            }
            /** @var \Omines\OAuth2\Client\Provider\Gitlab $user */
            $user = $client->fetchUserFromToken($accessToken);
        } catch (IdentityProviderException $e) {
            // something went wrong!
            // probably you should return the reason to the user
            return $this->render('dashboard/login.html.twig', [
                'error' => $e->getMessage()
            ]);
        }
    }
}
