<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\AssignmentRepository;
use App\Repository\ExerciseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class DashboardController extends AbstractController
{
    #[Route('/', name: 'app_dashboard')]
    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    public function index(
        #[CurrentUser] ?User $user,
        ExerciseRepository $exerciseRepository
    ): Response {
        $classrooms = $user->getClassrooms();
        // chargement du bon dashboard en fonction du rôle de l'utilisateur
        if (in_array('ROLE_TEACHER', $user->getRoles())) {
            return $this->render('dashboard/index.html.twig', [
                'classrooms' => $classrooms,
                'user' => $user
            ]);
        } else {
            $exercises = $exerciseRepository->findBy(['student' => $user]);
            return $this->render('dashboard/indexStudent.html.twig', [
                'classrooms' => $classrooms,
                'exercises' => $exercises,
                'user' => $user
            ]);
        }
    }
}
