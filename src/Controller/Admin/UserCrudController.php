<?php

namespace App\Controller\Admin;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('username')->hideOnForm(),
            TextField::new('gitLabUserId')->hideOnForm(),
            ImageField::new('avatarUrl')->hideOnForm(),
            ChoiceField::new('roles')->setChoices([
                'Student' => 'ROLE_STUDENT',
                'Teacher' => 'ROLE_TEACHER',
                'Admin' => 'ROLE_ADMIN',
            ])->allowMultipleChoices(),
            EmailField::new('email'),
        ];
    }
}
