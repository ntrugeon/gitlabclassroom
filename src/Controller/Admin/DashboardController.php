<?php

namespace App\Controller\Admin;

use App\Entity\Assignment;
use App\Entity\Classroom;
use App\Entity\Exercise;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class DashboardController extends AbstractDashboardController
{
    public function __construct(private TranslatorInterface $translator)
    {
    }

    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        /** @var AdminUrlGenerator $adminUrlGenerator */
        $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        return $this->redirect($adminUrlGenerator->setController(ClassroomCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Gitlabclassroom');
    }

    public function configureActions(): Actions
    {
        $actions = parent::configureActions();
        $actions->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
            return $action->setIcon('fa fa-edit')->setLabel(false);
        });

        $actions->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
            return $action->setIcon('fa fa-trash-alt')->setLabel(false);
        });

        return $actions;
    }

    public function configureCrud(): Crud
    {
        $crud =  parent::configureCrud();
        return $crud->showEntityActionsInlined();
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoRoute(
            $this->translator->trans('back_dashboard'),
            'fa fa-sign-out-alt',
            'app_dashboard'
        );
        yield MenuItem::section($this->translator->trans('Admin'), 'fa fa-cogs');
        yield MenuItem::linkToCrud(
            $this->translator->trans('User'),
            'fas fa-user',
            User::class
        );
        yield MenuItem::linkToCrud(
            $this->translator->trans('Classroom'),
            'fas fa-school',
            Classroom::class
        );
        yield MenuItem::linkToCrud(
            $this->translator->trans('Assignment'),
            'fas fa-book-open-reader',
            Assignment::class
        );
        yield MenuItem::linkToCrud(
            $this->translator->trans('Exercise'),
            'fas fa-laptop-code',
            Exercise::class
        );
    }
}
