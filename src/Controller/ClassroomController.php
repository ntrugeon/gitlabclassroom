<?php

namespace App\Controller;

use App\Entity\Classroom;
use App\Entity\User;
use App\Event\ClassroomEvent;
use App\Form\ClassroomType;
use App\Service\GitlabConnector;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ClassroomController extends AbstractController
{
    public function __construct(private readonly TranslatorInterface $translator)
    {
    }

    #[Route('/classroom/new', name:'app_classroom_new')]
    #[IsGranted('ROLE_TEACHER')]
    public function addClassroom(
        Request $request,
        GitlabConnector $connector,
        SluggerInterface $slugger,
        EntityManagerInterface $entityManager,
        EventDispatcherInterface $eventDispatcher,
        #[CurrentUser] ?User $user
    ): Response {
        $classroom = new Classroom();
        $form = $this->createForm(ClassroomType::class, $classroom);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $parentGroup = $form->get('parentGroup')->getData();
            // modification de la visibilité du groupe parent en internal
            if ($parentGroup && $parentGroup->id !== 0) {
                $connector->getClient()->groups()->update(
                    $parentGroup->id,
                    [
                        'visibility' => 'internal'
                    ]
                );
            }
            $newGroup = $connector->getClient()->groups()->create(
                (string)$slugger->slug($classroom->getName()),
                (string)$slugger->slug($classroom->getName()),
                'Gitlab group for the classroom ' . $classroom->getName(),
                parent_id: $parentGroup->id === 0 ? null : $parentGroup->id,
                visibility: 'internal'
            );
            $classroom->setGitlabGroupId($newGroup['id']);
            $classroom->setGitlabUrl($newGroup['web_url']);
            $connector->getClient()->projects()->create(
                'Private',
                [
                    'description' => 'Private repository for the classroom ' . $classroom->getName(),
                    'namespace_id' => $classroom->getGitlabGroupId(),
                    'visibility' => 'private'
                ]
            );
            $classroom->setOwner($user);
            $entityManager->persist($classroom);
            $entityManager->flush();
            $this->addFlash(
                'message',
                $this->translator->trans('classroom_created', ['%classroom%' => $classroom->getName()])
            );
            $eventDispatcher->dispatch(new ClassroomEvent($classroom));

            return $this->redirectToRoute('app_dashboard');
        }
        return $this->render('classroom/add.html.twig', [
            'form' => $form
        ]);
    }

    #[Route('/classroom/{id}/delete', name: 'app_classroom_delete')]
    #[isGranted('ROLE_TEACHER')]
    public function deleteClassroom(
        Classroom $classroom,
        EntityManagerInterface $entityManager,
        GitlabConnector $connector,
        #[CurrentUser] User $user,
        EventDispatcherInterface $eventDispatcher,
    ): Response {
        // vérifier que l'utilisateur est propriétaire de la classe
        if ($classroom->getOwner() === $user) {
            try {
                $entityManager->remove($classroom);
                $entityManager->flush();
                $eventDispatcher->dispatch(new ClassroomEvent($classroom, 'deleted'));
            } catch (\Exception $e) {
                $this->addFlash(
                    'error',
                    $this->translator->trans("cant_delete_classroom")
                );
                return $this->redirectToRoute('app_classroom_view', ['id' => $classroom->getId()]);
            }
            $connector->getClient()->groups()->remove($classroom->getGitlabGroupId());
            $this->addFlash(
                'message',
                $this->translator->trans('classroom_deleted', ['%classroom%' => $classroom->getName()])
            );
        } else {
            $this->addFlash(
                'message',
                $this->translator->trans('classroom_deleted_not_allowed')
            );
        }
        return $this->redirectToRoute('app_dashboard');
    }

    #[Route('/classroom/{id}', name: 'app_classroom_view')]
    #[isGranted('ROLE_TEACHER')]
    public function viewClassroom(Classroom $classroom): Response
    {
        $assignments = $classroom->getAssignments();
        return $this->render('classroom/view.html.twig', [
            'classroom' => $classroom,
            'assignments' => $assignments
        ]);
    }

    #[Route('/classroom/{id}/join', name: 'app_classroom_join')]
    #[isGranted('IS_AUTHENTICATED_FULLY')]
    public function joinClassroom(
        #[MapEntity(mapping: ['id' => 'id'])]
        Classroom $classroom,
        #[CurrentUser] ?User $user,
        GitlabConnector $connector,
        EntityManagerInterface $entityManager
    ) {
        $user->addClassroom($classroom);
        $entityManager->flush();
        if ($user->hasRole('ROLE_TEACHER')) {
            $teacher = $classroom->getOwner();
            $connector->getClient($teacher)->groups()->addMember(
                $classroom->getGitlabGroupId(),
                $user->getGitlabUserId(),
                GitlabConnector::ACCESS_MAINTAINER
            );
        }
        return $this->redirectToRoute('app_dashboard');
    }

    #[Route('/classroom/{id}/student/{username}', name: 'app_classroom_student_view')]
    #[isGranted('ROLE_TEACHER')]
    public function viewClassroomStudent(
        #[MapEntity(mapping: ['id' => 'id'])]
        Classroom $classroom,
        #[MapEntity(mapping: ['username' => 'username'])]
        User $user,
    ): Response {
        $exercises = $user->getExercises()->filter(function ($exercise) use ($classroom) {
            return $exercise->getAssignment()->getClassroom() === $classroom;
        });
        return $this->render('classroom/viewStudent.html.twig', [
            'classroom' => $classroom,
            'student' => $user,
            'exercises' => $exercises
        ]);
    }
}
