<?php

namespace App\Controller;

use App\DTO\Result;
use App\Entity\Assignment;
use App\Entity\Classroom;
use App\Entity\Exercise;
use App\Entity\User;
use App\Form\AssignmentType;
use App\Repository\ExerciseRepository;
use App\Repository\UserRepository;
use App\Service\GitlabConnector;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Random\RandomException;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class AssignmentController extends AbstractController
{
    public function __construct(private readonly TranslatorInterface $translator)
    {
    }

    #[Route('/assignment/{id}/accept', name: 'app_assignment_accept')]
    #[isGranted('ROLE_STUDENT')]
    public function acceptAssignment(
        Assignment $assignment,
        #[CurrentUser] ?User $user,
        EntityManagerInterface $entityManager,
        SluggerInterface $slugger,
        GitlabConnector $connector
    ): Response {
        $teacher = $assignment->getClassroom()->getOwner();
        $classroom = $assignment->getClassroom();
        if (!in_array($user, $classroom->getUsers()->toArray())) {
            $this->addFlash(
                'error',
                $this->translator->trans('assignment_cannot_join')
            );
            return $this->redirectToRoute('app_dashboard');
        }
        // vérifier si la date de début n'est pas encore arrivée ou la date de fin de l'assignement est dépassée
        // attention, la date de début ou de fin peuvent être null
        $now = new \DateTimeImmutable();
        if (
            $now < ($assignment->getStartDate() ?? new \DateTimeImmutable("-1day"))
            || $now > ($assignment->getEndDate() ?? new \DateTimeImmutable("+1day"))
        ) {
            $this->addFlash('error', $this->translator->trans('assignment_not_available'));
            return $this->redirectToRoute('app_dashboard');
        }
        $user->addAssignment($assignment);

        $group = $connector->getClient($teacher)->groups()->show($assignment->getGitlabGroupId());
        if ($assignment->getGitlabTemplateId() !== 0) {
            $newProject = $connector->getClient($teacher)->projects()->fork($assignment->getGitlabTemplateId(), [
                'name' => $slugger->slug($assignment->getName() . '-' . $user->getUserIdentifier()),
                'path' => $slugger->slug($assignment->getName() . '-' . $user->getUserIdentifier()),
                'namespace' => $group['id']
            ]);
        } else {
            $newProject = $connector->getClient($teacher)->projects()->create(
                $assignment->getName() . '-' . $user->getUserIdentifier(),
                [
                    'namespace_id' => $group['id']
                ]
            );
        }
        $connector->getClient($teacher)->projects()->addMember(
            $newProject['id'],
            $user->getGitlabUserId(),
            GitlabConnector::ACCESS_MAINTAINER
        );
        $exercise = new Exercise();
        $assignment->addUser($user);
        $exercise->setAssignment($assignment);
        $exercise->setStudent($user);
        $exercise->setGitlabProjectId($newProject['id']);
        $exercise->setGitlabProjectUrl($newProject['web_url']);
        $entityManager->persist($exercise);

        $entityManager->flush();
        return $this->redirectToRoute('app_dashboard');
    }

    #[Route('/assignement/{id}', name: 'app_assignment_view')]
    #[isGranted('ROLE_TEACHER')]
    public function viewAssignemnt(
        #[MapEntity(expr: 'repository.findOneSort(id)')]
        Assignment $assignment
    ): Response {
        $nbExericesDone = $assignment->getExercises()->filter(function (Exercise $exercise) {
            return $exercise->getScore() !== null;
        })->count();
        return $this->render('assignment/view.html.twig', [
            'assignment' => $assignment,
            'nbExericesDone' => $nbExericesDone
        ]);
    }

    #[Route('/assignement/{id}/delete', name: 'app_assignment_delete')]
    #[isGranted('ROLE_TEACHER')]
    public function deleteAssignemnt(
        Assignment $assignment,
        EntityManagerInterface $entityManager,
        GitlabConnector $connector,
        #[CurrentUser] User $user,
        Request $request
    ): Response {
        // vérifier que l'assignement appartient bien à l'utilisateur courant
        if ($assignment->getClassroom()->getUsers()->contains($user)) {
            try {
                $entityManager->remove($assignment);
                $entityManager->flush();
            } catch (\Exception $e) {
                $this->addFlash(
                    'error',
                    "You can't delete this assignment because it's already used by students."
                );
                return $this->redirectToRoute('app_assignment_view', ['id' => $assignment->getId()]);
            }
            $connector->getClient($user)->groups()->remove($assignment->getGitlabGroupId());
        }
        return $this->redirectToRoute('app_classroom_view', ['id' => $assignment->getClassroom()->getId()]);
    }

    #[Route('/assignement/{id}/export', name: 'app_assignment_export')]
    #[isGranted('ROLE_TEACHER')]
    public function exportAssignemnt(Assignment $assignment): Response
    {
        $exercises = $assignment->getExercises();
        return $this->render('assignment/export.html.twig', [
            'exercises' => $exercises
        ]);
    }

    #[Route('/assignement/{id}/edit', name: 'app_assignment_edit')]
    #[isGranted('ROLE_TEACHER')]
    public function editAssignemnt(
        Assignment $assignment,
        Request $request,
        EntityManagerInterface $entityManager,
    ): Response {
        $form = $this->createForm(
            AssignmentType::class,
            $assignment,
            [ 'is_edit' => true ]
        );
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // TODO: à voir si on peut changer le nom également (et donc le path et les forks) du groupe gitlab
            $entityManager->flush();
            return $this->redirectToRoute('app_assignment_view', ['id' => $assignment->getId()]);
        }
        return $this->render('assignment/edit.html.twig', [
            'form' => $form,
            'assignment' => $assignment
        ]);
    }

    #[Route('/classroom/{id}/assignment/new', name: 'app_classroom_assignment_new')]
    #[isGranted('ROLE_TEACHER')]
    public function addAssignment(
        Classroom $classroom,
        Request $request,
        GitlabConnector $connector,
        SluggerInterface $slugger,
        EntityManagerInterface $entityManager,
    ) {
        $assignment = new Assignment();
        $form = $this->createForm(
            AssignmentType::class,
            $assignment,
            [ 'gitlabId' => $classroom->getGitlabGroupId() ]
        );
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // TODO: il faut vérifier que le nom du template soit différent du nom de l'assignement
            $template = $form->get('template')->getData();
            $newGroup = $connector->getClient()->groups()->create(
                (string)$slugger->slug($assignment->getName()),
                (string)$slugger->slug($assignment->getName()),
                'Gilab group for the assigment ' . $assignment->getName(),
                parent_id: $classroom->getGitlabGroupId()
            );
            $assignment->setGitlabGroupId($newGroup['id']);
            $assignment->setGitlabTemplateUrl($template->web_url ?? '');
            $assignment->setClassroom($classroom);
            $assignment->setGitlabTemplateId($template->id);
            $entityManager->persist($assignment);
            $entityManager->flush();
            return $this->redirectToRoute('app_classroom_view', ['id' => $classroom->getId()]);
        }
        return $this->render('assignment/add.html.twig', [
            'form' => $form,
            'classroom' => $classroom
        ]);
    }

    /**
     * @throws RandomException
     */
    #[Route('/assignment/submit', name: 'app_assignment_submit', methods: ['POST'])]
    public function submitAssignment(
        Request $request,
        ExerciseRepository $exerciseRepository,
        UserRepository $userRepository,
        EntityManagerInterface $entityManager,
        Filesystem $filesystem,
        #[Autowire('%kernel.project_dir%/public/studentResults')] string $resultsDirectory
    ): Response {
        // TODO: en tant qu'enseignant, il faut pouvoir vérifier la CI avec moins de vérifications
        // on récupère le token_id de gitlab et on extrait les informations du projet
        $authHeader = $request->headers->get('Authorization');
        if (!$authHeader || !str_starts_with($authHeader, 'Bearer ')) {
            return new Response('Token JWT manquant ou invalide', Response::HTTP_UNAUTHORIZED);
        }

        $jwt = substr($authHeader, 7); // Supprimer "Bearer " pour obtenir le token
        try {
            // Séparer les parties du token JWT
            list($header, $payload, $signature) = explode('.', $jwt);
            unset($header, $signature);
            $decodedPayload = json_decode(base64_decode($payload), true);

            $projectId = $decodedPayload['project_id'];
            $userId = $decodedPayload['user_id'];
            $username = $decodedPayload['user_login'];

            // on peut vérifier si le .gitlab-ci.yml n'a pas été modifié en faisant un sha256sum du fichier
            // et en le comparant à sa valeur calculé et passé en variable gitlab du groupe.
        } catch (Exception $e) {
            return new Response(
                'Erreur lors du décodage du token JWT : ' . $e->getMessage(),
                Response::HTTP_UNAUTHORIZED
            );
        }

        // récupérer le fichier xml envoyé en POST par curl
        /** @var UploadedFile $file */
        $file = $request->files->get('file');

        if (!$file) {
            return new Response('Aucun fichier reçu', Response::HTTP_BAD_REQUEST);
        }

        $user = $userRepository->findOneBy(['gitlabUserId' => $userId, 'username' => $username]);

        if (!in_array($user->getRoles(), ['ROLE_TEACHER'])) {
            $exercice = $exerciseRepository->findOneBy(['gitlabProjectId' => $projectId]);

            if (!$exercice) {
                return new Response('Exercice non trouvé', Response::HTTP_NOT_FOUND);
            }

            // on vérifie que la date de fin de l'assignement n'est pas dépassée
            $now = new \DateTimeImmutable();

            if ($now > ($exercice->getAssignment()->getEndDate() ?? new \DateTimeImmutable("+1day"))) {
                return new Response('Date de fin de l\'assignement dépassée', Response::HTTP_NOT_ACCEPTABLE);
            }

            if (
                $exercice->getStudent()->getGitlabUserId() !== $userId &&
                $exercice->getStudent()->getUsername() !== $username
            ) {
                return new Response('Exercice non associé à cet utilisateur', Response::HTTP_FORBIDDEN);
            }
            $result = new Result($file->getPathname());
            $result->calculateScore();
            if (!$result->isSuccess()) {
                return new Response('Aucun test n\'a été effectué', Response::HTTP_PAYMENT_REQUIRED);
            }
            $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $newFilename = $originalFilename . '-' . random_bytes(8) . '-' . $username . '.' . $file->guessExtension();

            try {
                $file->move($resultsDirectory, $newFilename);
                if (
                    $exercice->getResultFile() !== null &&
                    $filesystem->exists($resultsDirectory . '/' . $exercice->getResultFile())
                ) {
                    $filesystem->remove($resultsDirectory . '/' . $exercice->getResultFile());
                }
            } catch (FileException $e) {
                return new Response(
                    'Erreur lors de l\'enregistrement du fichier',
                    Response::HTTP_INTERNAL_SERVER_ERROR
                );
            }
            $exercice->setTotal($result->getTotal());
            $exercice->setScore($result->getScore());
            $exercice->setCreatedAt(new \DateTimeImmutable());
            $exercice->setResultFile($newFilename);
            $exercice->setAttempt($exercice->getAttempt() + 1);
            $entityManager->flush();
        }
        return new Response('Fichier XML analysé avec succès', Response::HTTP_OK);
    }

    /**
     * Resets the grade of a specific exercise for students.
     *
     * This method allows a user to reset the score, total, and creation date of an exercise.
     * It includes access control, ensuring the user is authorized to perform the action.
     * If the user is a teacher, they will be redirected to the assignment view;
     * otherwise, they are redirected to the dashboard.
     *
     * @param Exercise $exercise The exercise to be reset.
     * @param EntityManagerInterface $entityManager The entity manager responsible for database persistence.
     * @param User $user The currently authenticated user.
     *
     * @return Response Redirects the user to either the assignment view or the dashboard.
     */
    #[Route('/exercise/{id}/student/reset', name: 'app_exercise_reset')]
    #[isGranted('ROLE_TEACHER')]
    public function resetExerciseGrade(
        Exercise $exercise,
        EntityManagerInterface $entityManager,
        #[CurrentUser] User $user,
    ): Response {
        if (
            $exercise->getStudent() !== $user
            && !in_array('ROLE_TEACHER', $user->getRoles())
            && !$exercise->getAssignment()->getClassroom()->getUsers()->contains($user)
        ) {
            return $this->redirectToRoute('app_dashboard');
        }
        $exercise->setScore(null);
        $exercise->setTotal(null);
        $exercise->setCreatedAt(null);
        $entityManager->flush();
        if (in_array('ROLE_TEACHER', $user->getRoles())) {
            return $this->redirectToRoute('app_assignment_view', [
                'id' => $exercise->getAssignment()->getId()
            ]);
        } else {
            return $this->redirectToRoute('app_dashboard');
        }
    }

    #[Route('/exercise/{id}', name: 'app_exercise_view')]
    #[isGranted('ROLE_USER')]
    public function viewExerciseGrade(
        Exercise $exercise,
        #[CurrentUser] User $user,
        #[Autowire('%kernel.project_dir%/public/studentResults')] string $resultsDirectory
    ): Response {
        if (
            $exercise->getStudent() !== $user
            && !in_array('ROLE_TEACHER', $user->getRoles())
            && !$exercise->getAssignment()->getClassroom()->getUsers()->contains($user)
        ) {
            return $this->redirectToRoute('app_dashboard');
        }
        $result = new Result($resultsDirectory . '/' . $exercise->getResultFile());
        $result->getContentToView();
        return $this->render('exercise/view.html.twig', [
            'exercise' => $exercise,
            'testcases' => $result->getTestcases()
        ]);
    }

    #[Route('/assignment/submit/test', name: 'app_assignment_submit_test', methods: ['POST'])]
    public function submitAssignmentTest(
        Request $request,
    ): Response {
        /** @var UploadedFile $file */
        $file = $request->files->get('file');

        if (!$file) {
            return new Response('Aucun fichier reçu', Response::HTTP_BAD_REQUEST);
        }

        $result = new Result($file->getPathname());
        if (!$result->isSuccess()) {
            return new Response('Aucun test n\'a été effectué', Response::HTTP_PAYMENT_REQUIRED);
        }
        return new Response('Fichier XML analysé avec succès', Response::HTTP_OK);
    }

    #[Route('/download/{filename}', name:"download_result_file")]
    #[isGranted('IS_AUTHENTICATED_FULLY')]
    public function downloadFileAction(
        string $filename,
        #[Autowire('%kernel.project_dir%/public/studentResults')] string $resultsDirectory
    ): Response {
        if (!file_exists($resultsDirectory . '/' . $filename)) {
            throw $this->createNotFoundException('The file does not exist');
        }
        $response = new BinaryFileResponse($resultsDirectory . '/' . $filename);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $filename);
        return $response;
    }

    public function getProjectDetails(GitlabConnector $connector): array
    {
        // show all branches
        dd($connector->getClient()->repositories()->branches(9728));
    }
}
