<?php

namespace App\Event;

use App\Entity\Classroom;
use Symfony\Contracts\EventDispatcher\Event;

final class ClassroomEvent extends Event
{
    public function __construct(protected Classroom $classroom, protected string $message = 'created')
    {
    }

    public function getClassroom(): Classroom
    {
        return $this->classroom;
    }

    public function getMessage(): string
    {
        return $this->message;
    }
}
