<?php

namespace App\Repository;

use App\Entity\Assignment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Uid\Uuid;

/**
 * @extends ServiceEntityRepository<Assignment>
 */
class AssignmentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Assignment::class);
    }

    public function findOneSort($id): ?Assignment
    {
        $uid = Uuid::fromString($id);
        // il faut ajouter le select des jointures pour que le tri se fasse correctement.

        return $this->createQueryBuilder('a')
            ->select('a', 'e', 's')
            ->leftJoin('a.exercises', 'e')
            ->leftJoin('e.student', 's')
            ->andWhere('a.id = :id')
            ->setParameter('id', $uid->toBinary())
            ->orderBy('s.username', 'ASC')
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    //    /**
    //     * @return Assignment[] Returns an array of Assignment objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('a')
    //            ->andWhere('a.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('a.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Assignment
    //    {
    //        return $this->createQueryBuilder('a')
    //            ->andWhere('a.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
