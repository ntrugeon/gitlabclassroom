<?php

namespace App\DataFixtures;

use App\Entity\Classroom;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\ORM\Doctrine\Populator;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');
        $populator = new Populator($faker, $manager);
        $populator->addEntity(
            Classroom::class,
            10,
            [
                'name' => function () use ($faker) {
                    return $faker->sentence(3);
                },
                'gitlabUrl' => function () use ($faker) {
                    return $faker->url();
                },
            ]
        );

        $populator->addEntity(
            User::class,
            10,
            [
                'username' => function () use ($faker) {
                    return $faker->userName();
                },
                'roles' => function () {
                    return ['ROLE_STUDENT'];
                },
                'gitlabUserId' => function () use ($faker) {
                    return $faker->randomNumber(8);
                },
                'avatarUrl' => function () use ($faker) {
                    return $faker->url();
                },
                'email' => function () use ($faker) {
                    return $faker->email();
                },
            ]
        );

        $populator->execute();
        $ntruge01 = new User();
        $ntruge01->setEmail('ntruge01@etudiant.univ-lr.fr');
        $ntruge01->setUsername('ntruge01');
        $ntruge01->setAvatarUrl($faker->url);
        $ntruge01->setRoles(['ROLE_STUDENT']);
        $ntruge01->setGitlabUserId(12);
        $ntruge01->setCreatedAtToken(time());
        $ntruge01->setExpiresToken(time());
        $ntruge01->setAccessToken('sdf');
        $ntruge01->setRefreshToken('gfd');

        $manager->persist($ntruge01);

        $ntrugeon = new User();
        $ntrugeon->setEmail('ntrugeon@univ-lr.fr');
        $ntrugeon->setUsername('ntrugeon');
        $ntrugeon->setAvatarUrl($faker->url);
        $ntrugeon->setRoles(['ROLE_TEACHER']);
        $ntrugeon->setGitlabUserId(13);
        $ntrugeon->setCreatedAtToken(time());
        $ntrugeon->setExpiresToken(time());
        $ntrugeon->setAccessToken('sqsdfsdfqds');
        $ntrugeon->setRefreshToken('gfdqsdfsdgdqg');


        $manager->persist($ntrugeon);
        $manager->flush();
    }
}
