<?php

namespace App\Entity;

use App\Repository\ExerciseRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ExerciseRepository::class)]
class Exercise
{
    #[ORM\Id]
    #[ORM\Column(type: UuidType::NAME, unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    private ?Uuid $id;

    #[ORM\Column]
    private ?int $gitlabProjectId = null;

    #[ORM\Column(length: 255)]
    #[Assert\Url()]
    private ?string $gitlabProjectUrl = null;

    #[ORM\ManyToOne(inversedBy: 'exercises')]
    private ?Assignment $assignment = null;

    #[ORM\ManyToOne(inversedBy: 'exercises')]
    private ?User $student = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\Column(nullable: true)]
    private ?int $score = null;

    #[ORM\Column(nullable: true)]
    private ?int $total = null;

    #[ORM\Column(options: ['default' => 0])]
    private ?int $attempt = 0;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $resultFile = null;

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getGitlabProjectId(): ?int
    {
        return $this->gitlabProjectId;
    }

    public function setGitlabProjectId(int $gitlabProjectId): static
    {
        $this->gitlabProjectId = $gitlabProjectId;

        return $this;
    }

    public function getGitlabProjectUrl(): ?string
    {
        return $this->gitlabProjectUrl;
    }

    public function setGitlabProjectUrl(string $gitlabProjectUrl): static
    {
        $this->gitlabProjectUrl = $gitlabProjectUrl;

        return $this;
    }

    public function getAssignment(): ?Assignment
    {
        return $this->assignment;
    }

    public function setAssignment(?Assignment $assignment): static
    {
        $this->assignment = $assignment;

        return $this;
    }

    public function getStudent(): ?User
    {
        return $this->student;
    }

    public function setStudent(?User $student): static
    {
        $this->student = $student;

        return $this;
    }

    public function __toString(): string
    {
        return $this->getStudent() . "- " . $this->getAssignment();
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(?int $score): static
    {
        $this->score = $score;

        return $this;
    }

    public function getTotal(): ?int
    {
        return $this->total;
    }

    public function setTotal(?int $total): static
    {
        $this->total = $total;

        return $this;
    }

    public function getAttempt(): ?int
    {
        return $this->attempt;
    }

    public function setAttempt(int $attempt): static
    {
        $this->attempt = $attempt;

        return $this;
    }

    public function getResultFile(): ?string
    {
        return $this->resultFile;
    }

    public function setResultFile(?string $resultFile): static
    {
        $this->resultFile = $resultFile;

        return $this;
    }
}
