<?php

namespace App\Entity;

use App\Repository\ClassroomRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ClassroomRepository::class)]
class Classroom
{
    #[ORM\Id]
    #[ORM\Column(type: UuidType::NAME, unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    private ?Uuid $id;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    #[Assert\Length(
        min: 5,
        minMessage: 'The name must be at least 5 characters long'
    )]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    #[Assert\Url()]
    private ?string $gitlabUrl = null;

    #[ORM\Column]
    private ?int $gitlabGroupId = null;

    /**
     * @var Collection<int, User>
     */
    #[ORM\ManyToMany(targetEntity: User::class, inversedBy: 'classrooms')]
    private Collection $users;

    /**
     * @var Collection<int, Assignment>
     */
    #[ORM\OneToMany(targetEntity: Assignment::class, mappedBy: 'classroom')]
    private Collection $assignments;

    #[ORM\ManyToOne(inversedBy: 'myclassrooms')]
    private ?User $owner = null;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->assignments = new ArrayCollection();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getGitlabUrl(): ?string
    {
        return $this->gitlabUrl;
    }

    public function setGitlabUrl(string $gitlabUrl): static
    {
        $this->gitlabUrl = $gitlabUrl;

        return $this;
    }

    public function getGitlabGroupId(): ?int
    {
        return $this->gitlabGroupId;
    }

    public function setGitlabGroupId(int $gitlabGroupId): static
    {
        $this->gitlabGroupId = $gitlabGroupId;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): static
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
        }

        return $this;
    }

    public function removeUser(User $user): static
    {
        $this->users->removeElement($user);

        return $this;
    }

    /**
     * @return Collection<int, Assignment>
     */
    public function getAssignments(): Collection
    {
        return $this->assignments;
    }

    public function addAssignment(Assignment $assignment): static
    {
        if (!$this->assignments->contains($assignment)) {
            $this->assignments->add($assignment);
            $assignment->setClassroom($this);
        }

        return $this;
    }

    public function removeAssignment(Assignment $assignment): static
    {
        if ($this->assignments->removeElement($assignment)) {
            // set the owning side to null (unless already changed)
            if ($assignment->getClassroom() === $this) {
                $assignment->setClassroom(null);
            }
        }

        return $this;
    }

    public function getStudents(): Collection
    {
        return new ArrayCollection(array_filter($this->users->toArray(), function ($user) {
            return $user->hasRole('ROLE_STUDENT');
        }));
    }

    public function getTeachers(): Collection
    {
        return new ArrayCollection(array_filter($this->users->toArray(), function ($user) {
            return $user->hasRole('ROLE_TEACHER');
        }));
    }

    public function nbStudents(): int
    {
        return count($this->getStudents());
    }

    public function nbTeachers(): int
    {
        // on ajoute le propriétaire de la classe
        return count($this->getTeachers()) + 1;
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): static
    {
        $this->owner = $owner;

        return $this;
    }
}
