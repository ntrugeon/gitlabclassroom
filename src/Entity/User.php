<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\UniqueConstraint(name: 'UNIQ_IDENTIFIER_USERNAME', fields: ['username'])]
class User implements UserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 180)]
    private ?string $username = null;

    /**
     * @var list<string> The user roles
     */
    #[ORM\Column]
    private array $roles = [];

    #[ORM\Column(type: Types::BIGINT)]
    private ?string $gitlabUserId = null;

    #[ORM\Column(length: 255)]
    #[Assert\Url()]
    private ?string $avatarUrl = null;

    #[ORM\Column(length: 255)]
    #[Assert\Email()]
    private ?string $email = null;

    /**
     * @var Collection<int, Classroom>
     */
    #[ORM\ManyToMany(targetEntity: Classroom::class, mappedBy: 'users')]
    private Collection $classrooms;

    /**
     * @var Collection<int, Assignment>
     */
    #[ORM\ManyToMany(targetEntity: Assignment::class, mappedBy: 'users')]
    private Collection $assignments;

    /**
     * @var Collection<int, Exercise>
     */
    #[ORM\OneToMany(targetEntity: Exercise::class, mappedBy: 'student')]
    private Collection $exercises;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $accessToken = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $refreshToken = null;

    #[ORM\Column(nullable: true)]
    private ?int $expiresToken = null;

    #[ORM\Column(nullable: true)]
    private ?int $createdAtToken = null;

    /**
     * @var Collection<int, Classroom>
     */
    #[ORM\OneToMany(targetEntity: Classroom::class, mappedBy: 'owner')]
    private Collection $myclassrooms;

    public function __construct()
    {
        $this->classrooms = new ArrayCollection();
        $this->assignments = new ArrayCollection();
        $this->exercises = new ArrayCollection();
        $this->myclassrooms = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): static
    {
        $this->username = $username;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->username;
    }

    /**
     * @see UserInterface
     *
     * @return list<string>
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * @param list<string> $roles
     */
    public function setRoles(array $roles): static
    {
        $this->roles = $roles;

        return $this;
    }

    public function hasRole(string $role): bool
    {
        return in_array($role, $this->getRoles());
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getGitlabUserId(): ?string
    {
        return $this->gitlabUserId;
    }

    public function setGitlabUserId(string $gitlabUserId): static
    {
        $this->gitlabUserId = $gitlabUserId;

        return $this;
    }

    public function getAvatarUrl(): ?string
    {
        return $this->avatarUrl;
    }

    public function setAvatarUrl(string $avatarUrl): static
    {
        $this->avatarUrl = $avatarUrl;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    public function getClassrooms(): array
    {
        return array_merge($this->classrooms->toArray(), $this->myclassrooms->toArray());
    }

    public function addClassroom(Classroom $classroom): static
    {
        if (!$this->classrooms->contains($classroom)) {
            $this->classrooms->add($classroom);
            $classroom->addUser($this);
        }
        return $this;
    }

    public function removeClassroom(Classroom $classroom): static
    {
        if ($this->classrooms->removeElement($classroom)) {
            $classroom->removeUser($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Assignment>
     */
    public function getAssignments(): Collection
    {
        return $this->assignments;
    }

    public function addAssignment(Assignment $assignment): static
    {
        if (!$this->assignments->contains($assignment)) {
            $this->assignments->add($assignment);
        }

        return $this;
    }

    public function removeAssignment(Assignment $assignment): static
    {
        $this->assignments->removeElement($assignment);

        return $this;
    }

    /**
     * @return Collection<int, Exercise>
     */
    public function getExercises(): Collection
    {
        return $this->exercises;
    }

    public function addExercise(Exercise $exercise): static
    {
        if (!$this->exercises->contains($exercise)) {
            $this->exercises->add($exercise);
            $exercise->setStudent($this);
        }

        return $this;
    }

    public function removeExercise(Exercise $exercise): static
    {
        if ($this->exercises->removeElement($exercise)) {
            // set the owning side to null (unless already changed)
            if ($exercise->getStudent() === $this) {
                $exercise->setStudent(null);
            }
        }

        return $this;
    }

    public function getAccessToken(): ?string
    {
        return $this->accessToken;
    }

    public function setAccessToken(?string $accessToken): static
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    public function getRefreshToken(): ?string
    {
        return $this->refreshToken;
    }

    public function setRefreshToken(?string $refreshToken): static
    {
        $this->refreshToken = $refreshToken;

        return $this;
    }

    public function getExpiresToken(): ?int
    {
        return $this->expiresToken;
    }

    public function setExpiresToken(?int $expiresToken): static
    {
        $this->expiresToken = $expiresToken;

        return $this;
    }

    public function getCreatedAtToken(): ?int
    {
        return $this->createdAtToken;
    }

    public function setCreatedAtToken(?int $createdAtToken): static
    {
        $this->createdAtToken = $createdAtToken;

        return $this;
    }

    public function __toString(): string
    {
        return $this->getUserIdentifier();
    }

    /**
     * @return Collection<int, Classroom>
     */
    public function getMyclassrooms(): Collection
    {
        return $this->myclassrooms;
    }

    public function addMyclassroom(Classroom $myclassroom): static
    {
        if (!$this->myclassrooms->contains($myclassroom)) {
            $this->myclassrooms->add($myclassroom);
            $myclassroom->setOwner($this);
        }

        return $this;
    }

    public function removeMyclassroom(Classroom $myclassroom): static
    {
        if ($this->myclassrooms->removeElement($myclassroom)) {
            // set the owning side to null (unless already changed)
            if ($myclassroom->getOwner() === $this) {
                $myclassroom->setOwner(null);
            }
        }

        return $this;
    }
}
