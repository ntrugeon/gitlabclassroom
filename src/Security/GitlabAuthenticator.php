<?php

namespace App\Security;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use KnpU\OAuth2ClientBundle\Security\Authenticator\OAuth2Authenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;
use Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

/**
 * @see https://symfony.com/doc/current/security/custom_authenticator.html
 */
class GitlabAuthenticator extends OAuth2Authenticator implements AuthenticationEntryPointInterface
{
    use TargetPathTrait;

    public const LOGIN_ROUTE = 'app_login';

    public function __construct(
        private ClientRegistry $clientRegistry,
        private EntityManagerInterface $entityManager,
        private RouterInterface $router,
        #[Autowire('%env(PREFIX_EMAIL_TEACHER)%')]
        private $emailPrefix,
    ) {
    }

    /**
     * Called on every request to decide if this authenticator should be
     * used for the request. Returning `false` will cause this authenticator
     * to be skipped.
     */
    public function supports(Request $request): ?bool
    {
        return $request->attributes->get('_route') === 'connect_gitlab_check';
    }

    public function authenticate(Request $request): Passport
    {
        $client = $this->clientRegistry->getClient('gitlab');
        $accessToken = $this->fetchAccessToken($client);

        return new SelfValidatingPassport(
            new UserBadge($accessToken->getToken(), function () use ($accessToken, $client) {
                $gitlabUser = $client->fetchUserFromToken($accessToken)->toArray();

                $email = $gitlabUser['email'];
                $gitlabId = $gitlabUser['id'];

                $existingUser = $this->entityManager->getRepository(User::class)->findOneBy([
                    'gitlabUserId' => $gitlabId
                ]);

                if ($existingUser) {
                    $existingUser->setAccessToken($accessToken->getToken());
                    $existingUser->setRefreshToken($accessToken->getRefreshToken());
                    $existingUser->setExpiresToken($accessToken->getExpires());
                    $existingUser->setCreatedAtToken(time());
                    $this->entityManager->flush();
                    return $existingUser;
                }

                $user = new User();

                $user->setGitlabUserId($gitlabId);
                $user->setEmail($email);
                if (str_contains($user->getEmail(), $this->emailPrefix)) {
                    $user->setRoles(['ROLE_TEACHER']);
                } else {
                    $user->setRoles(['ROLE_STUDENT']);
                }
                $user->setAvatarUrl($gitlabUser['avatar_url']);
                $user->setUsername($gitlabUser['username']);
                $user->setAccessToken($accessToken->getToken());
                $user->setRefreshToken($accessToken->getRefreshToken());
                $user->setExpiresToken($accessToken->getExpires());
                $this->entityManager->persist($user);
                $this->entityManager->flush();

                return $user;
            })
        );
    }

    public function onAuthenticationSuccess(
        Request $request,
        TokenInterface $token,
        string $firewallName
    ): ?Response {
        return new RedirectResponse(
            $this->getTargetPath($request->getSession(), $firewallName) ?? $this->router->generate('app_dashboard')
        );
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $data = [
            // you may want to customize or obfuscate the message first
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData()),

            // or to translate this message
            // $this->translator->trans($exception->getMessageKey(), $exception->getMessageData())
        ];
        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    public function start(Request $request, ?AuthenticationException $authException = null): Response
    {
        return new RedirectResponse($this->router->generate('app_login'), Response::HTTP_TEMPORARY_REDIRECT);
    }
}
