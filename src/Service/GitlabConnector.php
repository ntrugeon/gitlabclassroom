<?php

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Gitlab\Client;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Security\Core\User\UserInterface;

class GitlabConnector
{
    public const ACCESS_OWNER = 50;
    public const ACCESS_MAINTAINER = 40;
    public const ACCESS_DEVELOPER = 30;

    public function __construct(
        private Client $client,
        private Security $security,
        private ClientRegistry $clientRegistry,
        private EntityManagerInterface $entityManager,
        #[Autowire('%env(GITLAB_URL)%')]
        private $url
    ) {
    }

    public function getClient(User $user = null): Client
    {
        if (!$user) {
            $user = $this->getUser($this->security->getUser());
        }

        $accessToken = $this->clientRegistry->getClient('gitlab')->refreshAccessToken($user->getRefreshToken());

        $user = $this->modifyUser($user, $accessToken);

        $this->client->setUrl($this->url);

        $this->client->authenticate($user->getAccessToken(), Client::AUTH_OAUTH_TOKEN);

        return $this->client;
    }

    private function modifyUser(User $user, $accessToken): User
    {
        $user->setAccessToken($accessToken->getToken());
        $user->setRefreshToken($accessToken->getRefreshToken());
        $user->setExpiresToken($accessToken->getExpires());
        $user->setCreatedAtToken(time());
        $this->entityManager->flush();
        return $user;
    }

    /**
     * Returns the real User object instead of the Security component with the UserInterface
     * @throws EntityNotFoundException
     */
    private function getUser(UserInterface $user = null): User
    {
        if (null === $user || ! $user instanceof User) {
            throw new EntityNotFoundException(
                sprintf(
                    'Expected App\\Entity\\User, got %s',
                    $user === null ? 'null' : get_class($user)
                )
            );
        }

        return $user;
    }
}
