/** @type {import('tailwindcss').Config} */

const { iconsPlugin, getIconCollections } = require("@egoist/tailwindcss-icons")

module.exports = {
  content: [
    "./assets/**/*.js",
    "./src/Form/**/*.php",
    "./templates/**/*.html.twig",
  ],
  theme: {
    extend: {
      outlineWidth: {
        6: '6px'
      }
    },
    colors: {
      'primary': {
        40: "#1B275F",
        60: "#283A8F",
        80: "#364EBE",
        100: "#4361EE",
        120: "#6981F1",
        140: "#8EA0F5",
        160: "#B4C0F8",
        180: "#D9DFFC",
      },
      "dark": {
        1: "#14171C",
        2: "#121B26",
        3: "#1F2A37",
        4: "#374151",
        5: "#4B5563",
        6: "#6B7280",
        7: "#9CA3AF",
        8: "#D1D5DB",
        9: "#E5E7EB",
        10: "#F1F2F3",
      },
      "white": "#FFFFFF",
      "red": {
        DEFAULT: "#F23030",
        100: "#E10E0E",
        80: "#F56060",
        60: "#F23030",
        40: "#F56060",
      },
      "teal": {
        DEFAULT: "#0B76B7",
        "dark": "#1F587A",
        "light": "#35ABF3",
        "light-20": "#0B76B7",
      },
      "orange": {
        DEFAULT: "#FBBF24",
        "dark": "#D8A00F",
        "light": "#F9D67B",
        "light-20": "#FBBF24",
      },
      "green": {
        DEFAULT: "#22AD5C",
        "dark": "#1A8245",
        "light": "#82E6AC",
        "light-20": "#22AD5C"
      }
    },
    spacing: {
      '0': '0px',
      '1': '4px',
      '2': '8px',
      '3': '12px',
      '4': '16px',
      '5': '20px',
      '6': '24px',
      '8': '32px',
      '10': '40px',
      '12': '48px',
      '14': '56px',
      '16': '64px',
      '18': '72px',
      '20': '80px',
      '32': '8rem',
    }
  },
  plugins: [
    require('@tailwindcss/forms'),
    iconsPlugin({
      // Select the icon collections you want to use
      // You can also ignore this option to automatically discover all individual icon packages you have installed
      // If you install @iconify/json, you should explicitly specify the collections you want to use, like this:
      collections: getIconCollections(["mdi", "lucide"]),
      // If you want to use all icons from @iconify/json, you can do this:
      // collections: getIconCollections("all"),
      // and the more recommended way is to use `dynamicIconsPlugin`, see below.
    }),
  ],
}

