.php-tests-common:
  before_script: &composer
    - composer install
  cache: &keep_vendor
    paths:
      - vendor/
  image: jakzal/phpqa:php8.3-alpine
  rules: &only_php_files
    - if: $CI_COMMIT_TAG
      when: never
    - changes:
        - '**/*.php'

stages:
  - security
  - test
  - documentation
  - deploy

security-checker:
  allow_failure: false
  image: jakzal/phpqa:php8.3-alpine
  rules:
    - if: '$CI_COMMIT_TAG'  # Exclut les exécutions sur tag
      when: never
    - changes:
        - composer.lock
        - composer.json
  script:
    - local-php-security-checker  --path=./composer.lock
  stage: security

twig-lint:
  allow_failure: false
  before_script: *composer
  cache: *keep_vendor
  image: jakzal/phpqa:php8.3-alpine
  rules:
    - changes:
        - "templates/**/*.html.twig"
  script:
    - php bin/console lint:twig templates
  stage: test

phpcs:
  allow_failure: false
  image: jakzal/phpqa:php8.3-alpine
  rules: *only_php_files
  script:
    - phpcs -v --standard=PSR12 --ignore=./src/Kernel.php ./src
  stage: test

phpstan:
  allow_failure: true
  before_script: *composer
  cache: *keep_vendor
  image: jakzal/phpqa:php8.3-alpine
  rules: *only_php_files
  script:
    - composer global bin phpstan remove phpstan/extension-installer
    - phpstan analyse ./src --level 3
  stage: test

phpunit:
  allow_failure: false
  before_script:
    - composer install
    - php bin/console doctrine:database:drop --if-exists --force --env=test
    - php bin/console doctrine:database:create --env=test
    - php bin/console d:m:m --env=test --no-interaction
    - php bin/console d:f:l --env=test --no-interaction
    - npm install
    - npm run build
  image: nicolasunivlr/php:lp2024-amd64
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - changes:
        - '**/*.php'
  script:
    - php ./vendor/bin/phpunit --coverage-cobertura phpunit-coverage.xml --coverage-text --colors=never
  services:
    - alias: mariadb
      name: mariadb:10.11
  stage: test
  coverage: '/^\s*Lines:\s*\d+.\d+\%/'
  artifacts:
    when: always
    reports:
      coverage_report:
        coverage_format: cobertura
        path: phpunit-coverage.xml
  variables:
    DATABASE_URL: mysql://root:pass_test@mariadb:3306/myapptest?serverVersion=10.11.6-MariaDB
    MYSQL_DATABASE: myapptest
    MYSQL_PASSWORD: myapptest
    MYSQL_ROOT_PASSWORD: pass_test
    MYSQL_USER: myapptest
    XDEBUG_MODE: coverage

deploy:
  before_script:
    - mkdir -p ~/.ssh
    - eval $(ssh-agent -s)
    - echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
    - echo "${PRODUCTION_DEPLOYMENT_KEY}" | tr -d "\r" | ssh-add -
  image:
    entrypoint:
      - ''
    name: deployphp/deployer:v7
  rules:
    - if: $CI_COMMIT_TAG =~ "/^v[0-9.]+$/"
      when: on_success
  script:
    - php /bin/dep deploy -vvv
    - php /bin/dep database:migrate
  stage: deploy

pages:
  artifacts:
    paths:
      - public
  before_script:
    - pip install -r requirements.txt
  image: python:3-alpine
  rules:
    - changes:
        - docs/**/*
      if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
  script:
    - rm -rf public
    - mkdocs build
  stage: documentation
  tags:
    - pagelab