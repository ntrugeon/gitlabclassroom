# A propos de ce projet

Ce projet est un outil pour les enseignants qui souhaitent utiliser Gitlab pour gérer leurs cours.
Il est réalisé en Symfony 7.
Il utilise les bibliothèques suivantes :

- Gitlab Api Client
- Gitlab oauth2 client
- Easyadmin

## Réalisation

Nicolas Trugeon (enseignant au département informatique de La Rochelle Université).

Fortement inspiré de [https://gitlab.univ-lille.fr/gitlab-classrooms/gitlab-classrooms](https://gitlab.univ-lille.fr/gitlab-classrooms/gitlab-classrooms) réalisé sous Spring Boot.

Merci à Julien Wittouck pour sa disponibilité.