# Utilisation de GitLab CI pour évaluer les devoirs des étudiants

GitLab Classroom utilise GitLab CI pour évaluer les devoirs des étudiants. Cela permet de vérifier que les devoirs des étudiants respectent les consignes données par les enseignants.

Chaque langage de programmation a ses procédures de tests unitaires ou fonctionnels. Cependant, tous ont la possibilité de fournir un fichier compatible JUnit pour les tests.

C'est ce fichier que sera analysé par Gitlab Classroom pour évaluer les devoirs des étudiants.

Il faut donc envoyer un fichier xml compatible JUnit à GitLab Classroom pour que les tests soient évalués.

Le point d'entrée de l'application pour cela est `/assignment/submit`.

## Exemple d'appel à l'API

On exécute cela après les tests afin de ne pas être bloqué en cas d'erreur. 

```bash
after_script:
  - |
    if curl --fail -X POST https://lpmiaw.univ-lr.fr/gitlabclassroom/assignment/submit -H "Authorization: Bearer $ID_TOKEN" -F "file=@./nom_fichier.xml" ; then
      echo "✅ successfully uploaded xml file"
    else
      echo "❌ error when uploading xml file, skipping..."
      ERROR=1
    fi
```

Il faut que récupérer le token d'identification de la CI pour pouvoir envoyer le fichier xml. Ce token est disponible dans les variables d'environnement de la CI.

```yaml
# .gitlab-ci.yml
  id_tokens:
    ID_TOKEN:
      aud: 'URL de Gitlab Classroom'
```

Ce token permet de vérifier que c'est le bon utilisateur pour le bon projet qui envoie le fichier xml.

## Exemples de fichiers .gitlab-ci.yml

Vous trouverez à terme dans le [dépot](https://gitlab.univ-lr.fr/gitlab-classroom) de GitLab Classroom des exemples de fichiers `.gitlab-ci.yml` pour les langages les plus courants. (PHP, Java, Python, Javascript)

N'hésitez pas à contribuer à ce dépot pour ajouter des exemples pour d'autres langages.

Gitlab fournit également des exemples sur la [documentation officielle](https://docs.gitlab.com/ee/ci/testing/unit_test_report_examples.html).