# Installation de Gitlab Classroom sur votre serveur

## Prérequis

- PHP >= 8.2
- Composer
- Git
- Un serveur web (Apache, Nginx, etc.).
- Un serveur de base de données (MySQL, PostgreSQL, SQLite, etc.).
- Une instance de Gitlab pour votre organisation. Vous n'avez pas besoin d'être administrateur de l'instance Gitlab pour utiliser Gitlab Classroom.

## Installation

Pour installer Gitlab Classroom, vous pouvez utiliser les commandes suivantes :

```bash
composer install
```

## Configuration

Pour configurer Gitlab Classroom, vous devez créer le fichier `.env.local` à la racine du projet.
Pour obtenir le GITLAB_CLIENT_ID et le GITLAB_CLIENT_SECRET, il faut aller dans votre gitlab et créer une application pour Gitlab Classroom :

- Aller dans `User` > `Preferences` > `Applications`
- Ajouter un nom
- Pour le Redirect URI, mettre l'URL de votre Gitlab Classroom suivi de `/connect/gitlab/check`
    - ex : https://lpmiaw.univ-lr.fr/gitlabclassroom/connect/gitlab/check
- Sélectionner le scope `api`
- Lors de la sauvegarde l'application, vous obtiendrez le GITLAB_CLIENT_ID et le GITLAB_CLIENT_SECRET

```bash
DATABASE_URL=mysql://user:ChangeMe!@localhost:3306/gitlabclassroom?serverVersion=10.11.8-MariaDB

GITLAB_URL=https://gitlab.univ-xxx.fr
GITLAB_CLIENT_ID=
GITLAB_CLIENT_SECRET=
```

Dans le fichier .env, vous pouvez définir le suffixe des adresses mails des enseignants.

Par exemple, à La Rochelle, les enseignants ont une adresse de type : `prenom.nom@univ-lr.fr` et les étudiants `prenom.nom@etudiants.univ-lr.fr`

Si vous définissez `@univ-lr.fr`, les utilisateurs, avec une adresse mail se terminant par `@univ-lr.fr`, seront automatiquement enseignants.

Il faut avoir le rôle `ROLE_ADMIN` pour avoir accès au backoffice via la route /admin.

## Création de la base de données et des tables

```bash
bin/console doctrine:database:create
bin/console doctrine:migrations:migrate
```

Enjoy !
