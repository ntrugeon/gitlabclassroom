Quand on veut utiliser Gitlab de manière pédagogique, il est nécessaire de créer des groupes, des projets puis d'y ajouter des étudiants avec les bons droits... 

L'idée de Gitlab Classroom est de simplifier cette tâche pour les enseignants et les étudiants.

# Enseignant

## Créer une classe

L'application permet de facilement créer une nouvelle classe en spécifiant son nom, son groupe parent.

Cela va créer un groupe Gitlab avec le nom de la classe. Ce groupe sera créé à l'intérieur du groupe parent si on en a choisi un ou à la racine de Gitlab dans le cas contraire.

Il faudra placer dans ce groupe l'ensemble des projets servant de modèles pour les devoirs des étudiants.

Cela va créer également un projet nommé `private` dans le groupe de la classe. Ce projet sera privé et accessible uniquement par les enseignants du groupe de la classe.
Il permet par exemple d'utiliser des ressources communes à tous les enseignants de la classe ou d'utiliser les tableaux de bord, issues de Gitlab...

## Ajouter des étudiants à une classe

Dès qu'une classe est créée, on retrouve sur la page de la classe un lien d'invitation à la classe. Ce lien est à partager avec les étudiants.

Ce lien permet aux étudiants de s'inscrire à la classe, mais n'a pas d'action sur Gitlab. Il permet simplement de les ajouter à la classe et qu'ils puissent ensuite accepter un devoir.

## Ajouter un devoir à une classe basée sur un projet Gitlab

Pour ajouter un devoir à une classe, il faut spécifier son nom et le projet Gitlab qui servira de modèle pour le devoir. 

Il faut donc au préalable avoir créé un projet dans le groupe de la classe qui servira de modèle. 
Si aucun modèle n'est sélectionné, le devoir créera un projet vide pour chaque étudiant.

Il est possible de spécifier une date de début et de fin pour le devoir. Cela a pour conséquence de ne pas pouvoir rejoindre le devoir avant la date de début
et de ne pas pouvoir envoyer le résultat de la CI après la date de fin.

Au niveau de Gitlab, cela va créer un groupe au nom du devoir dans le groupe de la classe.

Un nouveau lien d'invitation du devoir est disponible pour les étudiants. Ce lien permet aux étudiants de s'inscrire au devoir et de forker automatiquement le projet modèle du devoir.
Chaque projet étudiant est au nom de l'étudiant et n'est accessible que par lui et les enseignants de la classe.

Une fois que le devoir est créé, il est possible de le supprimer si aucun étudiant ne l'a rejoint.
On peut à tout moment modifier sa date de début ou de fin.

##  Voir les étudiants qui ont accepté l'invitation de la classe ou des devoirs

Une fois que les étudiants ont accepté l'invitation à la classe ou aux devoirs, il est possible de voir dans le tableau de bord la liste des étudiants qui ont accepté l'invitation.

## Voir le résultat des tests de CI pour chaque étudiant

Les enseignants peuvent voir le résultat des tests de CI pour chaque étudiant. Cela permet de voir si les tests ont été passés avec succès ou non.

On peut voir la liste des étudiants pour chaque devoir et la liste des devoirs pour chaque étudiant.

Quand on clique sur un devoir d'un étudiant, on tombe sur une page récapitulative permettant de visualiser pour chaque test le succès (en vert) ou l'échec (en rouge) avec le message d'erreur associé.

On peut également télécharger le fichier de résultats des tests au format xml.

![detail fichier Xml](images/viewXml.png)

Un enseignant peut supprimer le résultat du dernier test, mais cela ne remet pas à 0 le nombre de tentatives.

## Récupérer les devoirs des étudiants

Pour chaque devoir, il est possible de récupérer les devoirs des étudiants. Cela fournit un script basique permettant de cloner l'ensemble des projets d'un devoir.

# Étudiant

## Accepter une invitation à une classe ou un devoir

Pour accéder à gitlab classroom, il faut d'abord accepter une invitation à une classe. Ensuite, on peut accepter l'invitation à un devoir. 
L'invitation à un devoir n'est pas possible si l'invitation à la classe n'a pas été acceptée.

## Voir les devoirs auxquels ils ont accès

Dans le tableau de bord de l'étudiant, il est possible de voir les devoirs auxquels il a accès regroupés par classe.

## Voir le résultat des tests de CI pour chaque devoir

Pour chaque devoir, l'étudiant peut voir le résultat des tests de CI. Cela permet de voir si les tests ont été passés avec succès ou non.

Il peut également télécharger le fichier de résultats des tests pour mieux comprendre les erreurs qu'il a commises.

Un étudiant peut supprimer le résultat de son dernier test, mais cela ne remet pas à 0 le nombre de tentatives.