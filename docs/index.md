# Gitlab Classroom

Bienvenue dans Gitlab Classroom, un outil pour les enseignants qui souhaitent utiliser Gitlab pour gérer leurs cours.

L'idée de Gitlab Classroom est de simplifier la gestion des groupes dans gitlab et de pouvoir retrouver facilement le travail des étudiants.

De ce point de départ est né Gitlab Classroom...

![tableau de bord](images/dashboardTeacher.png)

Actuellement, Gitlab Classroom est traduit en français, en espagnol et en anglais. La langue s'adapte automatiquement en fonction de la langue du navigateur.

Les grandes fonctionnalités de Gitlab Classroom sont :

> Pour les enseignants

- Créer des classes
- Ajouter des étudiants à une classe
- Ajouter des devoirs à une classe basée sur un projet Gitlab
- Voir les étudiants qui ont accepté l'invitation de la classe ou des devoirs
- Voir le résultat des tests de CI pour chaque étudiant

> Pour les étudiants

- Accepter une invitation à une classe ou un devoir.
- Voir les devoirs auxquels ils ont accès.
- Voir le résultat des tests de CI pour chaque devoir.

Propulsé par Symfony ♥