<?php

namespace App\Tests;


use App\Repository\ClassroomRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ClassroomControllerTest extends WebTestCase
{

    public function testTrue()
    {
        $this->assertTrue(true);
    }

/*
    public function testAddClassroom()
    {
        $client = static::createClient();

        $mockForm = $this->getMockBuilder(ClassroomType::class)
            ->disableOriginalConstructor()
            ->getMock();

        $userRepository = static::getContainer()->get(UserRepository::class);

        $ntrugeon = $userRepository->findOneByUsername('ntrugeon');
        $client->loginUser($ntrugeon);
        $session = new Session(new MockFileSessionStorage());
        $request = new Request();
        $request->setSession($session);
        $stack = self::getContainer()->get(RequestStack::class);
        $stack->push($request);
        $csrfToken = $client->getContainer()->get('security.csrf.token_manager')->getToken('classroom__token');

        $client->request('POST', '/classroom/new', [
            'classroom' => [
                'name' => 'Nouvelle Classe',
                'parentGroup' => 0,
                '_csrf_token' => $csrfToken,
            ]
        ]);

        // Vérifications des assertions
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertSelectorTextContains('.classroom-card', 'Nouvelle Classe');
    }
    */
    public function testDeleteClassroom()
    {
        $client = static::createClient();

        $userRepository = static::getContainer()->get(UserRepository::class);

        $ntrugeon = $userRepository->findOneByUsername('ntrugeon');
        $client->loginUser($ntrugeon);

        // create a classroom with mock
        $classroomRepository = static::getContainer()->get(ClassroomRepository::class);
        $classroom = $classroomRepository->findAll()[0];

        // Accès à la route de suppression
        $client->request('GET', '/classroom/' . $classroom->getId() . '/delete');
        // Vérifications
        $this->assertResponseRedirects('/'); // Exemple de redirection attendue après suppression
        $client->followRedirect();
    }
}
