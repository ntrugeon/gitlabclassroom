<?php

namespace App\Tests;

use App\Entity\User;
use App\Service\GitlabConnector;
use Doctrine\ORM\EntityManagerInterface;
use Gitlab\Client;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\SecurityBundle\Security;

class GitlabConnectorTest extends TestCase
{
    private $entityManager;
    private $gitlabClient;
    private $clientRegistry;
    private $security;
    //private $gitlabConnector;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->gitlabClient = $this->createMock(Client::class);
        $this->clientRegistry = $this->createMock(ClientRegistry::class);
        $this->security = $this->createMock(Security::class);

//        $this->gitlabConnector = new GitlabConnector(
//            $this->gitlabClient,
//            $this->security,
//            $this->clientRegistry,
//            $this->entityManager,
//            ''
//        );
    }

    public function testTrue(): void
    {
        $this->assertTrue(true);
    }

    /*public function testGetClient(): void
    {
        $mockUser = $this->createMock(User::class);
        $mockUser->setRefreshToken('some_token');
        $this->security
            ->method('getUser')
            ->willReturn($mockUser);

        // Configuration du mock pour le client GitLab
        $this->gitlabClient
            ->expects($this->once()) // On attend une seule fois son appel
            ->method('getClient') // Supposons que `getClient` appelle cette méthode
            ->with('some_token', Client::AUTH_HTTP_TOKEN);

        // Simuler l'utilisateur actuel dans la sécurité (si nécessaire)


        // Appeler la méthode
        $result = $this->gitlabConnector->getClient($mockUser);

        // Vérification du résultat
        $this->assertInstanceOf(Client::class, $result);
    }

    public function testGetClientWithUser(): void
    {
        $mockUser = $this->createMock(User::class);

        // Pas besoin de mocker getUser, car un utilisateur est fourni
        $result = $this->gitlabConnector->getClient($mockUser);

        $this->assertInstanceOf(Client::class, $result);
    }*/

}
