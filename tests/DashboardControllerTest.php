<?php

namespace App\Tests;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DashboardControllerTest extends WebTestCase
{
    public function testDashboardNotConnected(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertResponseRedirects('/login');
        $client->followRedirect();
        $this->assertResponseIsSuccessful();
    }

    public function testDashboardConnected(): void
    {
        $client = static::createClient();
        $userRepository = static::getContainer()->get(UserRepository::class);
        $ntrugeon = $userRepository->findOneByUsername('ntrugeon');
        $this->assertTrue(in_array('ROLE_TEACHER', $ntrugeon->getRoles()));
        $client->loginUser($ntrugeon);
        $crawler = $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
    }

    public function testDashboardAsStudent(): void
    {
        $client = static::createClient();
        $userRepository = static::getContainer()->get(UserRepository::class);

        // retrieve the test user
        $nicolas = $userRepository->findOneByUsername('ntruge01');

        // simulate $testUser being logged in
        $client->loginUser($nicolas);

        // test e.g. the profile page
        $client->request('GET', '/');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h4', 'Student');
        $this->assertTrue($nicolas->getUserIdentifier() === 'ntruge01');
        $this->assertTrue(in_array('ROLE_STUDENT', $nicolas->getRoles()));
    }
}
