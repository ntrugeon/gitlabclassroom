# Gitlab Classroom

## Avec le framework Symfony

## BUGS

- supprimer le fichier en cours avant de le changer ou garder le nom ?

## TODO

- [X] new classroom : permettre de filtrer les groupes parents
- [X] refresh token
- [X] partie admin pour gérer les entités sans passer directement par la BD
- [X] gestion de phpunit par retour de CI
- [X] ajout d'un groupe/projet privé dans la classe
- [X] faire le CD avec deployer
- [X] améliorer le CI (double vérification des tests lors d'un tag)
- [X] supprimer un cours non commencé
- [X] supprimer un TP non commencé
- [X] script pour télécharger les TPs des étudiants
- [X] montrer le score de l'exercice à l'étudiant
- [X] ajoute un lien pour aller sur le modèle de l'assignment
- [X] envoyer un mail à l'admin lors de la création d'un cours
- [X] ajout du nombre de tentatives pour un TP
- [X] ajout d'une date de début fin pour un assignment
- [X] ajout d'un propriétaire pour une classe
- [X] ajout d'une vue pour l'analyse des résultats (enseignant et étudiant)
- [ ] amélioration du dashboard de l'étudiant : voir les classrooms puis aller sur les TPs
- [X] multi languages
- [X] documentation
- [ ] Vérification de la gestion de junit/jest/python.test ... par retour de CI
- [ ] proposer des exemples de CI dans différents langages
- [X] ajouter un lien pour edit un assignment
- [ ] envoyer un mail à l'étudiant lors de la création d'un TP avec le lien pour accepter l'invitation
- [ ] envoyer un mail à l'étudiant lors de publication d'une correction
- [ ] ajouter une préférence pour les notifications dans le profil

## Remerciements

Ce projet est la réécriture du projet [Gitlab Classroom](https://gitlab.univ-lille.fr/gitlab-classrooms/gitlab-classrooms) de Julien Wittouck en php (avec Symfony).
Merci pour sa disponibilité.

## Installation, usage et autre...

- voir la [documentation](https://gitlabclassroom-bd4640.pagelab.univ-lr.fr/)

## Pistes de réflexion

### Travo

* Pour les professeurs

Donner l'accès aux étudiants à une correction avec possibilité de spécifier un niveau de permission, qui est 20 par défaut.

Retirer l'accès aux étudiants à une correction

Ajoute un commentaire sur une Merge Request d'un projet étudiant. Il est nécessaire de spécifier à chaque fois les identifiants du projet étudiant et de la Merge Request.

Quitter l'intégralité des TPs forkés ???

Vérifier que la CI n'a pas été modifiée par l'étudiant

Vérifier que les tests n'ont pas été modifiés par l'étudiant

### Université suisse
 
- Group Assignments : create an assignment for a group of students, with 1 repository for the assignment
- Exports :
  - Export the grades of an assignment in some usable format (like CSV or JSON)
  - Export a script to `git clone` all the assignments of a student, or all assignments
- Archive :
  - Archive an assignment manually, or schedule an archive : archiving an assignment sets the GitLab repository to read-only
  - Archive a classroom to remove it from the dashboard
- Assignments :
  - Add open/close dates management, to allow accepting assignments in a specific time-frame
  - Improve performance of New Classroom screen
- For Students :
  - Ability to see the list of assignments they haven't accepted yet
- Mailing / notifications
  - Email the students when an assignment is available / ready
- Teachers
  - Share a classroom between multiple teachers
- Docs
  - Rewrite the docs, with more usage documentation for the teacher
- for Autograding
  - Add a way to compute a Sonarqube Quality Gate score to get a grade for an assignment
  - Make the pipeline clear about what it does (better logs), to help students understand the status (red/green)